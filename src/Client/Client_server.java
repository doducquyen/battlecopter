/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Client;

import WatingRoom.ChooseRoom;
import WatingRoom.DisplayDesign;
import Server.Bullets;
import Server.Attackers;
import Server.Booms;
import com.google.gson.Gson;
import Play.Game;
import Play.ResolveLocalObj;
import Play.Background;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

public class Client_server implements Runnable{
    private TransferMethod transferMethod;
    private String choosedRoom = null;
    public boolean isIngame = false;
    public Client_server(Socket SOCK)
    {
        this.SOCK = SOCK;
        transferMethod = new TransferMethod(SOCK);
    }
    public void remote_roomUI(DisplayDesign roomUI)
    {
        this.roomUI = roomUI;
    }
    public void remote_lobby(ChooseRoom Lobby)
    {
        this.Lobby = Lobby;
    }
    public void remote_Login_frame(Play login_frame)
    {
        this.login_frame = login_frame;
    }
    public void remote_Game(Game game)
    {
        this.game = game;
    }
    public void remote_inGame(Background ingame)
    {
        this.ingame = ingame;
    }
    public String getchoosedRoom()
    {
        return choosedRoom;
    }
    public void setChoosedRoom(String s)
    {
        choosedRoom = s;
    }
    public TransferMethod getTransferMethod()
    {
        return transferMethod;
    }

    public boolean isWaittingServer()
    {
        return waitting;
    }

    public void setNotWaitting()
    {
        waitting = false;
    }

    public void setWaitting()
    {
        waitting = true;
        while(isWaittingServer())System.out.print("");
    }
    public void setClientName(String s)
    {
        client_name =s;
    }
    public String getName()
    {
        return client_name;
    }
    public void update_room() throws IOException
    {
        int ii = transferMethod.RECEIVE_i();
        
        listRoom.clear();
        for(int i=0;i<ii;i++){
            listRoom.add(transferMethod.RECEIVE_s());
        }
        setNotWaitting();
    }

    public void setEnableJoinButton(boolean active)
    {
        Lobby.setEnableJoinButton(active);
    }

    @Override
    public void run() {
        while(true)
        {
            try {
                String REQUIRE = transferMethod.RECEIVE_s();
                if(REQUIRE== null) return;
//                System.out.println(REQUIRE);
                
                    if(REQUIRE.compareTo("LOGIN_FALSE") == 0) {
                        login_frame.LoginSuccess(false);
                    } 
                    else if(REQUIRE.compareTo("LOGIN_SUCCESS") == 0) {
                        login_frame.LoginSuccess(true);
                        update_room();
                        while(Lobby == null) System.out.print("");
                        Lobby.update_list_room();
                    }
                    else if(REQUIRE.compareTo("JOIN_FALSE") == 0) {
                        setNotWaitting();                       
                    }
                    else if(REQUIRE.compareTo("JOIN_SUCCESS") == 0) {
                        int temp = transferMethod.RECEIVE_i();
                        setNotWaitting();
                        Lobby.openRoomFrame(temp,false);                        
                    }
                    else if(REQUIRE.compareTo("UPDATE_ROOM") == 0) {
                        update_room();
                        while(Lobby == null) System.out.print("");
                        Lobby.update_list_room();                       
                    }
                
                    else if(REQUIRE.compareTo("CREATE_SUCCESS") == 0) {
                        setNotWaitting();
                        Lobby.openRoomFrame(0,true);   
                    }
                    else if(REQUIRE.compareTo("JOIN_ROOM") == 0) {
                        roomUI.setEnemyInfo(transferMethod.RECEIVE_s());                 
                    }
                    else if(REQUIRE.compareTo("OUT_ROOM") == 0) {
                        OUT_ROOM();         
                    }
                    else if(REQUIRE.compareTo("READY") == 0) {
                        roomUI.setStartButtonEnable(transferMethod.RECEIVE_i());                
                    }
                    else if(REQUIRE.compareTo("START") == 0) {
                        String start_json = transferMethod.RECEIVE_s();
                        setNotWaitting();
                        START_GAME(start_json);                
                    }
                    else if(REQUIRE.compareTo("ANIMATION") == 0) {
                        String plane=transferMethod.RECEIVE_s(),bullets=transferMethod.RECEIVE_s();
                        String enemies=transferMethod.RECEIVE_s();String explodes = transferMethod.RECEIVE_s();
                        DO_ANIMATION(plane,bullets,enemies,explodes);
                    }
                    
            } catch (IOException ex) {}
        }
    }
    private void OUT_ROOM()
    {
        if(isIngame == true)
        {
            isIngame = false;
            roomUI.setVisible(true);
            game.dispose();
            game = null;
        }
        roomUI.setEnemyInfo(null);
        roomUI.checkBoss(true);
    }

    private void START_GAME(String s)
    {
        Gson gson = new Gson();
        ResolveLocalObj ol = gson.fromJson(s, ResolveLocalObj.class);
        roomUI.PlayGame(ol);
        isIngame = true;
    }
    public void gameover()
    {
        game.dispose();
        roomUI.setVisible(true);
    }
    public void logout()
    {
        if(Lobby != null) Lobby.dispose();
        Lobby = null;
    }

    private void DO_ANIMATION(String plane,String bullets,String enemies,String explodes)
    {
        Gson gson = new Gson();
        ResolveLocalObj ol = gson.fromJson(plane,ResolveLocalObj.class);
        Bullets bs = gson.fromJson(bullets,Bullets.class);
        Attackers e = gson.fromJson(enemies,Attackers.class);
        Booms explo = gson.fromJson(explodes, Booms.class);
        ingame.animation(ol,bs,e,explo);
    }

    public ArrayList listRoom = new ArrayList();
    private ChooseRoom Lobby;
    private Play login_frame;
    private DisplayDesign roomUI;
    private boolean waitting = true;
    private Socket SOCK;
    private String client_name;
    private Game game;
    private Background ingame;
}
