package WatingRoom;

import Client.Client_server;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.FlowLayout;
import javax.swing.JLabel;

public class ChooseRoom extends JFrame {

    private JPanel contentPane;
    private ProfileDesign profilePanel;
    private ListRoomDesign roomListPanel;
    private ActionDisplayDesign methodPanel;
    private Client_server client;
    
    public ChooseRoom(Client_server client) {
        this.client = client;
        init();
        this.client.remote_lobby(this);
    }
    
    public void setEnableJoinButton(boolean active)
    {
        methodPanel.setJoinButton(active);
    }
    
    private void init()
    {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 1000, 600);
        setLocationRelativeTo(null);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new FlowLayout(FlowLayout.CENTER, 50, 50));       
        roomListPanel = new ListRoomDesign(client);
        contentPane.add(roomListPanel);     
        methodPanel = new ActionDisplayDesign(client);
        contentPane.add(methodPanel);
        this.pack();
    }
    
    public void openRoomFrame(int i,boolean isBoss)
    {
        new WatingRoom.DisplayDesign(client,i,isBoss).setVisible(true);
        client.remote_lobby(null);
        this.dispose();
    }
    
    public void update_list_room()
    {
        roomListPanel.removeAllRoom();
        roomListPanel.addRooms(client.listRoom);
    } 
}
