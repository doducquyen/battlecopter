/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WatingRoom;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class ResizeIcon {
    
    static public int black = 1;
    static public int white = 0;
    static public String[] Icon={"Images/MB_1.png","Images/MB_2.png","Images/Bullet.png","Images/MB_Enemy.png","Images/Dynamic.png"};
    
    public static void setIcon(JLabel label, String fileName)
    {
        try {
            BufferedImage image = ImageIO.read(ResizeIcon.class.getClassLoader().getResource(fileName));
            int dx=0; int dy=0;
            
            int ix =image.getWidth(); int x =label.getSize().width;
            int iy =image.getHeight(); int y =label.getSize().height;
            
            if(x /y <= ix /iy)
            {
                dx=x;
                dy=dx*iy/ix;           
            }
            else
            {
                dy=y;
                dx=dy*ix /iy;
            }
            ImageIcon icon = new ImageIcon(image.getScaledInstance(dx, dy, BufferedImage.SCALE_SMOOTH));
            label.setIcon(icon);
        } catch (IOException ex) {
            Logger.getLogger(ResizeIcon.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void setIcon(JLabel label, int i)
    {
        try {
            BufferedImage image = ImageIO.read(ResizeIcon.class.getClassLoader().getResource(ResizeIcon.Icon[i]));
            
            int dx=0; int dy=0;
            int ix =image.getWidth(); int x =label.getSize().width;
            int iy =image.getHeight(); int y =label.getSize().height;
    
            if(x /y <= ix /iy)
            {
                dx=x;
                dy=dx*iy/ix;
            }
            else
            {  
                dy=y;
                dx=dy*ix /iy;
            }
            ImageIcon icon = new ImageIcon(image.getScaledInstance(dx, dy, BufferedImage.SCALE_SMOOTH));
            label.setIcon(icon);
        } catch (IOException ex) {
            Logger.getLogger(ResizeIcon.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ResizeIcon.Icon[i]);
        }
    }
}
