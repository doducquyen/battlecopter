/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GameServer {

    /**
     * @param args the command line arguments
     */
    static ListRoom room_client_list = new ListRoom();
    public static void main(String[] args) {
        final int port = 8888;
        try {
            ServerSocket SERVER = new ServerSocket(port);
            System.out.println("waiting client connect !");
            while(true)
            {
                Socket SOCK = SERVER.accept();
                System.out.println("connect success "+ SOCK.getPort());
                SocketMethod SOCK_ID = new SocketMethod();
                SOCK_ID.SERVER_ACCEPT_SocketID(SOCK);
                Server_client ser_cli = new Server_client(SOCK_ID, room_client_list);
                Thread x = new Thread(ser_cli);
                x.start();
            }
        } catch (IOException ex) {
            Logger.getLogger(GameServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
