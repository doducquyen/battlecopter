/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class ListRoom {
    private HashMap<String, SocketMethod> Client_list;
    private HashMap<String, SocketMethod> Playing_client;
    private HashMap<SocketMethod,ResolveActionInRoom> Room_list;
    public ListRoom() {
        Client_list = new HashMap<String, SocketMethod>();
        Room_list = new HashMap<SocketMethod,ResolveActionInRoom>();
        Playing_client = new HashMap<String,SocketMethod>();
    }

    public void addClient_list(String s,SocketMethod SOCK)
    {
        Client_list.put(s, SOCK);
    }
    private void addRoom_list(SocketMethod SOCK, ResolveActionInRoom Room)
    {
        Room_list.put(SOCK, Room);
    }
    public void ClientToRoom(String s,ResolveActionInRoom room)
    {
        SocketMethod SOCK = removeClient(s);
        Playing_client.put(s, SOCK);
        addRoom_list(SOCK, room);
    }
    public void RoomToClient(SocketMethod SOCK,boolean boo)
    {
        if(boo) Client_list.put(SOCK.getName(), SOCK);
        else SOCK.closePort();
        Playing_client.remove(SOCK.getName());
        Room_list.remove(SOCK);
    }


    public String[] getAllRoomName()
    {
        HashMap<ResolveActionInRoom,String> temp= new HashMap<ResolveActionInRoom, String>();
        int tempi=0;
        Set keys = Room_list.keySet();
        for(Iterator i = keys.iterator(); i.hasNext();)
        {
            SocketMethod socktemp = (SocketMethod) i.next();
            ResolveActionInRoom roomtemp = Room_list.get(socktemp);
            temp.put(roomtemp, "A");
        }
        String[] array = new String[temp.size()];
        keys = temp.keySet();
        for(Iterator i = keys.iterator();i.hasNext();)
        {
            ResolveActionInRoom roomtemp = (ResolveActionInRoom) i.next();
            array[tempi]=roomtemp.getBoss().getName(); tempi++;
        }
        return array;
    }

    public HashMap getClient_list()
    {
        return Client_list;
    }

    public HashMap getRoom_list()
    {
        return Room_list;
    }

    public SocketMethod getClient(String key)
    {
        return Client_list.get(key);
    }

    public ResolveActionInRoom getRoom(SocketMethod SOCK)
    {
        return Room_list.get(SOCK);
    }
    public ResolveActionInRoom getRoom(String s)
    {
        SocketMethod SOCK = Playing_client.get(s);
        return Room_list.get(SOCK);
    }

    private SocketMethod removeClient(String s)
    {
        return Client_list.remove(s);
    }
}
