/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;

public class Attacker {
    private int local[];
    private int speed;
    private boolean dead;

    public Attacker(int x,int y,int speed)
    {
        local = new int[2];
        local[0]=x;
        local[1]=y;
        dead = false;
        this.speed =speed;
    }

    public void setY(int y)
    {
        local[1]=y;
    }

    public int getX()
    {
        return local[0];
    }

    public int getY()
    {
        return local[1];
    }

    public int speed()
    {
        return speed;
    }

    public void translate()
    {
        if(getY()<355)
            local[1]+=1;
    }
    public void dead()
    {
        dead = true;
    }

    public boolean isDead()
    {
        return dead;
    }
}
